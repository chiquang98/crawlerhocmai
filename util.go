package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"strings"
)

func getHTMLPage(url string) *goquery.Document{
	res,err:=http.Get(url)
	if err!=nil{
		println("ERROR GET")
		return nil
	}
	defer res.Body.Close()
	if res.StatusCode !=200{
		println("Error res status")
		return nil
	}
	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)

	if err != nil {
		return nil
	}
	return doc
}
func checkError(err error) {
	if err != nil {
		print("Error: ")
		log.Println(err)
	}
}
//func (teachers Teachers)getAllTeacherInformation(doc *goquery.Document,f *os.File){
//	var wg sync.WaitGroup
//	doc.Find(".gv-image a").Each(func(i int, s *goquery.Selection){
//		wg.Add(1)
//		teacherLink, _ := s.Attr("href")
//		fmt.Println(teacherLink)
//		go teachers.getTeacherInformation(teacherLink,&wg,f)
//	})
//	wg.Wait()
//}
func getTeacherInformation(url string)  {
	//defer wg.Done()
	res := getHTMLPage(url)
	if res==nil{
		return
	}
	name :=res.Find(".work").Find("p::nth-child(0)").Text()
	fmt.Println(res.Find(".work p .text:first-child").Text())

	workplace :=strings.TrimSpace(res.Find(".work:first-child p ").Text())
	degree:=strings.TrimSpace(res.Find(".degree:first-child p").Text())
	subject:=strings.TrimSpace(res.Find(".subject:first-child p").Text())
	email :=strings.TrimSpace(res.Find(".email:first-child p a").Text())
	facebook :=strings.TrimSpace(res.Find(".facebook:first-child p a").Text())
	//splitRS := strings.Split(url,"/")

	//id:="idTeacher"+splitRS[4]
	//teacher :=Teacher{
	//	ID: id,
	//	subject: subject,
	//	degree: degree,
	//	Email: email,
	//	FacebookUrl: facebook,
	//}
	//teacherJSON,err :=json.Marshal(teacher)
	//checkError(err)
	//io.WriteString(f, string(teacherJSON)+"\n")
	//teachers.TotalTeacher++
	//teachers.List = append(teachers.List,teacher)
	fmt.Println(name)
	fmt.Println(workplace)
	fmt.Println(subject)
	fmt.Println(degree)
	fmt.Println(email)
	fmt.Println(facebook)

}
func (teachers Teachers)  getNextURL(doc *goquery.Document)string{
	nextPageLink, _ := doc.Find(".paging:last-child").Attr("href")
	// Trường hợp không có url
	if nextPageLink == "" {
		println("End of Category")
		return ""
	}
	return nextPageLink
}
func main() {
	getTeacherInformation("https://hocmai.vn/giao-vien/96/thay-bui-gia-noi.html")
}

