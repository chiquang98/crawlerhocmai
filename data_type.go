package main
type Category struct {
	Title string `json:"title"`
	URL string `json:"url"`
}
type Categories struct{
	Total int `json:"total"`
	List  []Category `json:"categories"`

}
type Teacher struct{
	ID string `json:"id"`
	//Name string `json:"name"`
	//WorkPlace string `json:"workplace"`
	subject string `json:"subject"`
	degree string `json:"degree"`
	Email string `json: "email"`
	FacebookUrl string `json:"facebookurl"`
}
type Teachers struct{
	TotalTeacher int `json:"totalTeacher"`
	TotalPage int `json:"totalpage"`
	List []Teacher `json:"Teachers""`
}
func newTeachers() *Teachers{
	return &Teachers{}
}
func newCategories() *Categories  {
	return &Categories{}
}
