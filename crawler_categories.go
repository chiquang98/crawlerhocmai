package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io/ioutil"
)

func (categories *Categories) getAllCategories(doc *goquery.Document) {
	doc.Find(".w-100").Each(func(i int, s *goquery.Selection) {
		catLink, _ := s.Attr("action")
		fmt.Println(catLink)
		catTitle, _ := s.Attr("aria-label")

		category := Category{
			Title: catTitle,
			URL:   catLink,
		}

		categories.Total++
		categories.List = append(categories.List, category)
	})
}
// run this function to crawlAllCategories
func crawlAllCategories() {
	categories := newCategories()
	res := getHTMLPage("https://hocmai.vn/")
	categories.getAllCategories(res)
	fmt.Println(categories)
	userJSON, err := json.Marshal(categories)
	checkError(err)
	err = ioutil.WriteFile("./output/categories.json", userJSON, 0644) // Ghi dữ liệu vào file JSON
	checkError(err)
}
func main() {
	crawlAllCategories()
}